#  QGLVIEWER_FOUND - system has matlab libraries
#  QGLVIEWER_INCLUDE_DIR - the matlab include directory
#  QGLVIEWER_LIBRARIES - link these to use matlab

FIND_PATH( QGLVIEWER_INCLUDE_DIR QGLViewer/qglviewer.h /usr/local/opt/libqglviewer )
FIND_LIBRARY( QGLVIEWER_LIBRARY QGLViewer /usr/local/opt/libqglviewer)

FIND_PACKAGE_HANDLE_STANDARD_ARGS( QGLVIEWER DEFAULT_MSG
    QGLVIEWER_INCLUDE_DIR
    QGLVIEWER_LIBRARY )

IF(QGLVIEWER_FOUND)
    SET( QGLVIEWER_LIBRARIES ${QGLVIEWER_LIBRARY} )
ENDIF()
