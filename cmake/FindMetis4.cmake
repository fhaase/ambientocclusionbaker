#  METIS4_FOUND - system has matlab libraries
#  METIS4_INCLUDE_DIR - the matlab include directory
#  METIS4_LIBRARIES - link these to use matlab

FIND_PATH( METIS4_INCLUDE_DIR metis.h /usr/local/opt/metis4/include )
FIND_LIBRARY( METIS4_LIBRARY metis /usr/local/opt/metis4/lib )

FIND_PACKAGE_HANDLE_STANDARD_ARGS( METIS4 DEFAULT_MSG
    METIS4_INCLUDE_DIR
    METIS4_LIBRARY )

IF(METIS4_FOUND)
    SET( METIS4_LIBRARIES ${METIS4_LIBRARY} )
ENDIF()
