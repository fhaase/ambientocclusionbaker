#pragma once

#include "TriMesh.h"
#include "QString"

#include <random>

class QGLViewer;


class MeshKD
{

public:

    typedef trimesh::vec Vec;
    typedef trimesh::TriMesh::Face Face;

    struct Ray {
        Vec d;
        Vec invd;
        Vec o;
    };

    struct BBox {
        Vec min;
        Vec max;
    };

    struct Cell {
        BBox bbox;
        Cell* right;
        Cell* left;
        bool leaf;
        std::vector<Face> triangles;
    };

    MeshKD();
    trimesh::TriMesh mesh_;
    std::vector<Vec> avgdir_;

    bool load(QString path);

    void buildKD();
    void genAO(int nsamples, float maxDistance);
    void genAOPerVertex(int nsamples);

    void draw(bool lighting, bool ao);
    void drawWireframe();

    void takeScreenshot(QString path);
    void writeAO(QString path);

    //DEBUG
    void drawKdTree(int depth);
private:
    double EPSILON =  1e-6;
    float maxDist_;
    Cell root_;

    //KD-Tree
    void setBBox(Cell *cell);
    void divideCell(Cell* cell);
    void sortFaces(Cell* cell, int axis);
    Vec faceCenter(const Face f) const;

    bool intersects(const Ray r);
    bool intersects(const Ray r, const Cell *cell) const;
    int triangle_intersection(const MeshKD::Vec V1,const MeshKD::Vec V2,const MeshKD::Vec V3,const MeshKD::Vec O,const MeshKD::Vec D, float *out) const;

    //Ray Generation
    std::vector<Ray>* genRays(const Vec pos, const Vec norm, int nsamples);

    Vec UniformSampleSphere(const float u1, const float u2) const;
    float VanDerCorput(uint32_t n, uint32_t scramble) const;
    float Sobol2(uint32_t n, uint32_t scramble) const;
    void Sample02(uint32_t n, const uint32_t scramble[], float sample[]) const;

    std::uniform_int_distribution<uint32_t> uintdistribution_;
    const float OneMinusEpsilon=0x1.fffffep-1;
    //DEBUG
    void drawSamples();
    void drawCell(const Cell& cell);

};
