#include <QtWidgets/QtWidgets>
#include <QtOpenGL/QtOpenGL>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    setlocale(LC_NUMERIC,"C");

    QGLFormat glf = QGLFormat::defaultFormat();
    glf.setSampleBuffers(true);
    glf.setSamples(4);
    QGLFormat::setDefaultFormat(glf);
    QApplication::setOrganizationDomain("cg.tu-berlin.de");
    QApplication::setOrganizationName("cgtub");
    QApplication::setApplicationName("Ambient Occlusion Baker");

    MainWindow mainWin;
    mainWin.show();

    return app.exec();
}
