#include "meshkd.h"

#include "qglviewer.h"
#include <limits>
#include <chrono>
#include <unordered_set>
#include "GL/freeglut.h"
#include "QOpenGLFramebufferObject"

MeshKD::MeshKD()
{
}

bool MeshKD::load(QString path)
{

    trimesh::TriMesh* ptr = trimesh::TriMesh::read(path.toStdString());
    if ( ptr != NULL ){
        std::cout << "Mesh: " << path.toStdString() << " loaded: " << ptr->faces.size() << " Faces\n" << std::endl;
        mesh_ = *ptr;
        mesh_.need_normals();

        return true;
    } else{
        return false;
    }

}

void MeshKD::genAO(int nsamples, float maxDistance)
{
    maxDist_ = maxDistance*mesh_.feature_size();
    std::cout << maxDist_ << std::endl;
    mesh_.colors.clear();
    mesh_.colors.resize(mesh_.vertices.size(), trimesh::Color(0,0,0));

    std::vector<int> valences;
    valences.resize(mesh_.vertices.size(),0);

    avgdir_.clear();
    avgdir_.resize(mesh_.vertices.size(), Vec(0,0,0));

    for (int i = 0; i < mesh_.faces.size(); i++ ){
        Face f = mesh_.faces.at(i);
        Vec center = Vec(0,0,0);
        Vec normal = Vec(0,0,0);
        for ( int v : f.v ){
            center += mesh_.vertices.at(v);
            normal += mesh_.normals.at(v);
        }
        center *= 1.0/3.0;
        normal *= 1.0/3.0;

        std::vector<Ray>* rays = genRays(center, normal, nsamples);

        double nclear = 0;
        Vec avgClear = Vec(0,0,0);

#pragma omp parallel for
        for ( int r = 0; r < nsamples; r++){
            Ray ray = rays->at(r);

            if ( ! intersects(ray) ){
                avgClear += ray.d ;
                nclear += 1;//( normal.dot( ray.d));
            }
        }

        double ao = double(nclear) / double(nsamples);
        trimesh::normalize(avgClear);

        for (int v : f.v){
            mesh_.colors.at(v) += trimesh::Color(ao,ao,ao);
            valences.at(v) += 1;
            avgdir_.at(v) += avgClear;
        }

        delete rays;

        if ( i % (mesh_.faces.size()/10) == 10)
        {
            std::cout << (100*i)/mesh_.faces.size() << "% ... ";
            fflush(stdout);
        }
    }
    std::cout << std::endl;

    for ( int i = 0 ; i < mesh_.colors.size(); i++){
        mesh_.colors.at(i) /= double(valences.at(i));
        //Experimental correction
        //double val = mesh_.colors.at(i)[0];
        //val = pow(val, 1.0/2.2);
        //mesh_.colors.at(i) = trimesh::Color(val,val,val);
        trimesh::normalize(avgdir_.at(i));
    }
}

void MeshKD::genAOPerVertex(int nsamples)
{
    mesh_.colors.clear();
    mesh_.colors.resize(mesh_.vertices.size(), trimesh::Color(0,0,0));

    std::vector<int> valences;
    valences.resize(mesh_.vertices.size(),0);

    for (int i = 0; i < mesh_.vertices.size(); i++ ){
        Vec center = mesh_.vertices.at(i);
        Vec normal = mesh_.normals.at(i);

        std::vector<Ray>* rays = genRays(center, normal, nsamples);

        int unoccluded = 0;
        Vec avgUnoccluded = Vec(0,0,0);

#pragma omp parallel for
        for ( int r = 0; r < nsamples; r++){
            Ray ray = rays->at(r);

            if ( ! intersects(ray) ){
                avgUnoccluded += ray.d;
                unoccluded ++;
            }
        }

        double ao = double(unoccluded) / double(nsamples);

        mesh_.colors.at(i) = trimesh::Color(ao,ao,ao);

        delete rays;

        if ( i % (mesh_.faces.size()/10) == 10)
        {
            std::cout << (100*i)/mesh_.vertices.size() << "% ... ";
            fflush(stdout);
        }
    }
    std::cout << std::endl;

}

void MeshKD::draw(bool lighting, bool ao)
{
    if ( lighting ){
        glEnable(GL_LIGHTING);
    }else{
        glDisable(GL_LIGHTING);
    }

    glShadeModel(GL_SMOOTH);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_DOUBLE, sizeof(mesh_.vertices[0]), &mesh_.vertices[0][0]);

    if (!mesh_.normals.empty() ) {
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_DOUBLE, sizeof(mesh_.normals[0]), &mesh_.normals[0][0]);
    } else {
        glDisableClientState(GL_NORMAL_ARRAY);
    }
    if (ao && !mesh_.colors.empty()) {
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(3, GL_DOUBLE, sizeof(mesh_.colors[0]), &mesh_.colors[0][0]);
    } else {
        glDisableClientState(GL_COLOR_ARRAY);
    }
    glDrawElements(GL_TRIANGLES, 3*mesh_.faces.size(), GL_UNSIGNED_INT, &mesh_.faces[0][0]);

    //drawBBoxes(&root_, 3);

}

void MeshKD::drawWireframe()
{
    glPolygonMode(GL_FRONT, GL_LINE);
    GLfloat c[4];
    glGetFloatv(GL_CURRENT_COLOR, c);
    glColor3f(1,0,0);
    glPolygonOffset(-1,-1);
    glEnable(GL_POLYGON_OFFSET_LINE);

    draw(false,false);

    glColor4fv(c);
    glDisable(GL_POLYGON_OFFSET_LINE);
    glPolygonMode(GL_FRONT, GL_FILL);
}

void MeshKD::takeScreenshot(QString path)
{
    unsigned int width = 1000;
    unsigned int height = 1000;

    QOpenGLFramebufferObjectFormat format;
    format.setSamples(4);
    format.setInternalTextureFormat(GL_RGB);
    format.setAttachment(QOpenGLFramebufferObject::Depth);
    QOpenGLFramebufferObject fbo(width, height, format);
    fbo.bind();

//    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//    int viewport[4];
//    glGetIntegerv(GL_VIEWPORT, viewport);
//    glViewport(0,0,1000,1000);

//    glMatrixMode(GL_PROJECTION);
//    glPushMatrix();
//    glLoadIdentity();
//    gluPerspective(30.0,1.0,1.0,2000.0);
//    glMatrixMode(GL_MODELVIEW);
//    glPushMatrix();
//    glLoadIdentity();
//    gluLookAt( 0, 0, -8,0.0, 1.0, 0.0,0.0, 1.0, 0.0 );

    draw(false,true);

    QImage screenshot = fbo.toImage().mirrored(true,false);

    bool success = screenshot.save(path,"PNG",100);
    std::cout << success << std::endl;

//    glMatrixMode(GL_MODELVIEW);
//    glPopMatrix();
//    glMatrixMode(GL_PROJECTION);
//    glPopMatrix();
//    glViewport(viewport[0],viewport[1],viewport[2],viewport[3]);
}

void MeshKD::writeAO(QString path)
{
    FILE *f = NULL;

    f = fopen(path.toStdString().c_str(), "wb");
    if (!f) {
        printf("Error opening [%s] for writing: %s.\n", path.toStdString().c_str(),
            strerror(errno));
        return;
    }

    std::string before_vert = " ";
    for ( int i = 0; i < mesh_.colors.size(); i++){
        fprintf(f, "%s%.7g %.7g %.7g %.7g\n", before_vert.c_str(),
                avgdir_[i][0],
                avgdir_[i][1],
                avgdir_[i][2],
                mesh_.colors[i][0]);
    }

    fclose(f);

}

int MeshKD::triangle_intersection(const Vec V1,  const Vec V2,  const Vec V3, //Triangle vertices
                                  const Vec O,  const Vec D,  //Ray direction
                                  float* out ) const
{
    MeshKD::Vec e1, e2;  //Edge1, Edge2
    MeshKD::Vec P, Q, T;
    double det, inv_det, u, v;
    double t;

    e1 = V2 - V1;
    e2 = V3 - V1;
    //Begin calculating determinant - also used to calculate u parameter
    P = D.cross(e2);
    //if determinant is near zero, ray lies in plane of triangle
    det = e1.dot(P);
    //NOT CULLING
    if(det > -EPSILON && det < EPSILON) return 0;
    inv_det = 1.0 / det;

    //calculate distance from V1 to ray origin
    T = O - V1;

    //Calculate u parameter and test bound
    u = T.dot(P) * inv_det;
    //The intersection lies outside of the triangle
    if(u < 0.0 || u > 1.0) return 0;

    //Prepare to test v parameter
    Q = T.cross(e1);

    //Calculate V parameter and test bound
    v = D.dot(Q) * inv_det;
    //The intersection lies outside of the triangle
    if(v < 0.0 || u + v  > 1.0) return 0;

    t = e2.dot(Q) * inv_det;
    if(t > EPSILON) { //ray intersection
        *out = t;
        return 1;
    }

    // No hit, no win
    return 0;
}

bool MeshKD::intersects(const MeshKD::Ray r)
{
    return intersects(r, &root_);
}

bool MeshKD::intersects(const Ray r, const MeshKD::Cell * cell) const
{
    if ( cell->leaf ){
        for ( Face f : cell->triangles){
            Vec v1 = mesh_.vertices.at(f.v[0]);
            Vec v2 = mesh_.vertices.at(f.v[1]);
            Vec v3 = mesh_.vertices.at(f.v[2]);
            float out;
            if ( triangle_intersection(v1,v2,v3,r.o, r.d, &out) ){
                if ( maxDist_ == 0 || out < maxDist_ )
                    return true;
            }
        }
        return false;
    }else{
        double t1 = (cell->bbox.min[0] - r.o[0])*r.invd[0];
        double t2 = (cell->bbox.max[0] - r.o[0])*r.invd[0];
        double t3 = (cell->bbox.min[1] - r.o[1])*r.invd[1];
        double t4 = (cell->bbox.max[1] - r.o[1])*r.invd[1];
        double t5 = (cell->bbox.min[2] - r.o[2])*r.invd[2];
        double t6 = (cell->bbox.max[2] - r.o[2])*r.invd[2];

        double tmin = fmax(fmax(fmin(t1, t2), fmin(t3, t4)), fmin(t5, t6));
        double tmax = fmin(fmin(fmax(t1, t2), fmax(t3, t4)), fmax(t5, t6));

        if (tmax < 0 || tmin > tmax )
        {
            return false;
        }
        if ( maxDist_ > 0 && tmin > maxDist_ ){
            return false;
        }

        return intersects(r,cell->left) || intersects(r, cell->right);
    }
}

inline float MeshKD::VanDerCorput(uint32_t n, uint32_t scramble) const{
    // Reverse bits of _n_
    n = (n << 16) | (n >> 16);
    n = ((n & 0x00ff00ff) << 8) | ((n & 0xff00ff00) >> 8);
    n = ((n & 0x0f0f0f0f) << 4) | ((n & 0xf0f0f0f0) >> 4);
    n = ((n & 0x33333333) << 2) | ((n & 0xcccccccc) >> 2);
    n = ((n & 0x55555555) << 1) | ((n & 0xaaaaaaaa) >> 1);
    n ^= scramble;
    return std::min(((n>>8) & 0xffffff) / float(1 << 24), OneMinusEpsilon);
}

inline float MeshKD::Sobol2(uint32_t n, uint32_t scramble) const{
    for (uint32_t v = 1 << 31; n != 0; n >>= 1, v ^= v >> 1)
        if (n & 0x1) scramble ^= v;
    return std::min(((scramble>>8) & 0xffffff) / float(1 << 24), OneMinusEpsilon);
}
inline void MeshKD::Sample02(uint32_t n, const uint32_t scramble[2], float sample[2]) const{
    sample[0] = VanDerCorput(n, scramble[0]);
    sample[1] = Sobol2(n, scramble[1]);
}

MeshKD::Vec MeshKD::UniformSampleSphere(float u1, float u2) const {
    float z = 1.f - 2.f * u1;
    float r = sqrtf(std::max(0.f, 1.f - z*z));
    float phi = 2.f * M_PI * u2;
    float x = r * cosf(phi);
    float y = r * sinf(phi);
    return Vec(x, y, z);
}


std::vector<MeshKD::Ray> *MeshKD::genRays(const Vec pos, const Vec norm, int nsamples)
{

    std::vector<Ray>* rays = new std::vector<Ray>();
    rays->resize(nsamples);

    std::mt19937 mt;

    uint32_t scramble[2] = { (uint32_t) uintdistribution_(mt), (uint32_t) uintdistribution_(mt) };
    float u[2];

    for( int i = 0; i < nsamples; i++){
        Ray ray;

        Sample02(i, scramble, u);
        Vec w = UniformSampleSphere(u[0], u[1]);
        if (w.dot(norm) < 0.) w = -w;

        Vec dir = w;

        ray.d = trimesh::normalize(dir);
        ray.invd = Vec(1.0/ray.d[0],1.0/ray.d[1],1.0/ray.d[2]);
        ray.o = pos;

        rays->at(i) = ray;
    }
    return rays;
}

void MeshKD::buildKD()
{
    root_ = Cell();
    root_.triangles.resize(mesh_.faces.size() );
    std::copy( mesh_.faces.begin(), mesh_.faces.end(), root_.triangles.begin());
    setBBox(&root_);
    divideCell(&root_);
}

void MeshKD::setBBox(Cell* cell)
{
    double dbl_max = std::numeric_limits<double>::lowest();
    Vec max = Vec(dbl_max,dbl_max,dbl_max);
    Vec min = Vec(-dbl_max,-dbl_max,-dbl_max);

    std::unordered_set<int> vertices = std::unordered_set<int>();
    for ( Face f : cell->triangles ){
        vertices.insert(f.v[0]);
        vertices.insert(f.v[1]);
        vertices.insert(f.v[2]);
    }

    for ( int i : vertices){
        Vec vert = mesh_.vertices.at(i);
        if ( min[0] > vert[0]){
            min[0] = vert[0];
        }
        if ( min[1] > vert[1]){
            min[1] = vert[1];
        }
        if ( min[2] > vert[2]){
            min[2] = vert[2];
        }
        if ( max[0] < vert[0]){
            max[0] = vert[0];
        }
        if ( max[1] < vert[1]){
            max[1] = vert[1];
        }
        if ( max[2] < vert[2]){
            max[2] = vert[2];
        }
    }

    cell->bbox.max = max;
    cell->bbox.min = min;

}

void MeshKD::divideCell(MeshKD::Cell* cell)
{
    if ( cell->triangles.size() < 10 ){
        cell->leaf = true;
        return;
    }
    cell->leaf = false;

    //Find axis to split
    Vec diff = cell->bbox.max - cell->bbox.min;
    int axis = 0;
    if ( fabs(diff[1]) >= fabs(diff[0]) && fabs(diff[1]) >= fabs(diff[2]) ){
        axis = 1;
    } else if ( fabs(diff[2]) >= fabs(diff[0]) && fabs(diff[2]) >= fabs(diff[1]) ){
        axis = 2;
    }

    sortFaces(cell, axis);
    int half = cell->triangles.size()/ 2;

    Cell* left = new Cell;
    left->triangles.resize(half);
    std::copy(cell->triangles.begin(), cell->triangles.begin() + half, left->triangles.begin());
    setBBox(left);
    cell->left = left;

    Cell* right = new Cell;
    right->triangles.resize(cell->triangles.end() - (cell->triangles.begin() + half)  );
    std::copy(cell->triangles.begin() + half, cell->triangles.end(), right->triangles.begin());
    setBBox(right);
    cell->right = right;

    divideCell(left);
    divideCell(right);
}

void MeshKD::sortFaces(MeshKD::Cell *cell, int axis)
{
    auto comp = [&](const Face m,const Face n)-> bool {
        return faceCenter(m)[axis] < faceCenter(n)[axis];
    };

    std::sort(cell->triangles.begin(), cell->triangles.end(), comp);
}

MeshKD::Vec MeshKD::faceCenter(const MeshKD::Face f) const
{
    Vec center = Vec(0,0,0);
    for ( int v : f.v){
        center += mesh_.vertices[v];
    }
    center *= 1.0/3.0;
    return center;
}

//DEBUG STUFF

void MeshKD::drawSamples()
{

    Vec center = Vec(0,0,0);
    Vec normal = Vec(1,0,0);
    int nsamples = 1000;
    std::vector<Ray>* rays = genRays(center, normal, nsamples);

    glDisable(GL_LIGHTING);
    glPolygonMode(GL_FRONT, GL_LINE);

    glBegin(GL_POINTS);
    for ( Ray r : *rays){
        //glVertex3d(r.o[0], r.o[1], r.o[2]);
        glVertex3d(r.d[0], r.d[1], r.d[2]);
    }
    glEnd();

    glDisable(GL_LIGHTING);
    glPolygonMode(GL_FRONT, GL_FILL);
}

void MeshKD::drawKdTree(int depth)
{
    if ( root_.triangles.empty() )
        return;

    std::vector<Cell> toDraw;
    toDraw.push_back(root_);
    for ( int i = 0; i < depth; i++){
        std::vector<Cell> newToDraw;
        for ( Cell c : toDraw ){
            if ( c.leaf ){
                //newToDraw.push_back(c);
            }else{
                newToDraw.push_back( *(c.left));
                newToDraw.push_back( *(c.right));
            }
        }
        std::swap(toDraw,newToDraw);
    }
    for (const Cell c : toDraw ){
        drawCell(c);
    }
}

void MeshKD::drawCell(const MeshKD::Cell &cell)
{
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    Vec max = cell.bbox.max;
    Vec min = cell.bbox.min;
    Vec center = 0.5*(max+min);
    Vec s = 0.5*(max - min);
    glTranslatef(center[0],center[1],center[2]);
    glScalef( fabs(s[0]), fabs(s[1]), fabs(s[2]));

    glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    glDisable(GL_LIGHTING);
    glBegin(GL_QUADS);
    glColor3f(0,1,0);
    glVertex3f(-1, -1, -1);
    glVertex3f(-1, -1,  1);
    glVertex3f(-1,  1,  1);
    glVertex3f(-1,  1, -1);

    glVertex3f( 1, -1, -1);
    glVertex3f( 1, -1,  1);
    glVertex3f( 1,  1,  1);
    glVertex3f( 1,  1, -1);

    glVertex3f(-1, -1, -1);
    glVertex3f(-1, -1,  1);
    glVertex3f( 1, -1,  1);
    glVertex3f( 1, -1, -1);

    glVertex3f(-1,  1, -1);
    glVertex3f(-1,  1,  1);
    glVertex3f( 1,  1,  1);
    glVertex3f( 1,  1, -1);

    glVertex3f(-1, -1, -1);
    glVertex3f(-1,  1, -1);
    glVertex3f( 1,  1, -1);
    glVertex3f( 1, -1, -1);

    glVertex3f(-1, -1,  1);
    glVertex3f(-1,  1,  1);
    glVertex3f( 1,  1,  1);
    glVertex3f( 1, -1,  1);

    glColor3f(1,1,1);
    glEnd();
    glPolygonMode(GL_FRONT,GL_FILL);
    glPolygonMode(GL_BACK,GL_NONE);
    glEnable(GL_LIGHTING);

    glPopMatrix();
}
