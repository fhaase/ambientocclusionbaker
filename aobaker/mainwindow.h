#pragma once

#include <QtWidgets/QtWidgets>
#include <memory>
#include "vec.h"
#include "meshkd.h"

class QGLViewer;


class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    MeshKD mesh_;

public Q_SLOTS:
    void draw();
    void initDraw();
    void loadMesh();
    void doBatch();
    void generateAO();
    void saveMesh();
    void takeScreenshot();
    void saveAO();

signals:
    void meshLoaded(bool loaded);
    void aoLoaded(bool loaded);
    
private:
    void initGLView();
    void initControlBar();

    QHBoxLayout *cLayout_;
    QWidget *rightBar_;
    QCheckBox* renderWireframe_;
    QCheckBox* renderAO_;
    QCheckBox* renderLighting_;
    QSpinBox* nSamples_;
    QDoubleSpinBox* maxDistance_;
    QSpinBox* debugDepth_;
        
    QGLViewer *viewer_;  
    
    QString meshPath_;

};
