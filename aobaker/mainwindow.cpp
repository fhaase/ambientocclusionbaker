﻿#include "mainwindow.h"

#include "qglviewer.h"
#include "TriMesh_algo.h"
#include "chrono"
#include <QtConcurrent/QtConcurrent>


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    QWidget* central = new QWidget(this);
    
    cLayout_ = new QHBoxLayout;
    setCentralWidget(central);
    central->setLayout(cLayout_);

    initGLView();
    initControlBar();

}

MainWindow::~MainWindow(){

}

void MainWindow::initGLView(){
    viewer_ = new QGLViewer();
    connect(viewer_, SIGNAL(drawNeeded()), this, SLOT(draw()));
    connect(viewer_, SIGNAL(viewerInitialized()), this, SLOT(initDraw()));
    cLayout_->addWidget(viewer_);
}

void MainWindow::initControlBar(){
    rightBar_ = new QWidget();
    rightBar_->setFixedWidth(200);
    cLayout_->addWidget(rightBar_);
    
    //Buttons
    
    QGroupBox *meshBox = new QGroupBox(tr("Mesh Data"));
    QVBoxLayout *meshLayout = new QVBoxLayout;
    
    QPushButton* loadButton = new QPushButton("Load Mesh");
    connect(loadButton, SIGNAL(clicked()), this, SLOT(loadMesh()));

    QPushButton* doBatchButton = new QPushButton("Do Batch");
    connect(doBatchButton, SIGNAL(clicked()), this, SLOT(doBatch()));

    QLabel* nSamplesLabel = new QLabel("Number of Samples");
    nSamples_ = new QSpinBox();
    nSamples_->setMinimum(1);
    nSamples_->setMaximum(100000);
    nSamples_->setValue(1024);

    QLabel* distanceLabel = new QLabel("maxDistance/featureSize");
    maxDistance_ = new QDoubleSpinBox();
    maxDistance_->setMinimum(0);
    maxDistance_->setMaximum(100000);
    maxDistance_->setValue(0);
    maxDistance_->setSpecialValueText("Infinite");

    QPushButton* generateAOButton = new QPushButton("Generate AO");
    connect(generateAOButton, SIGNAL(clicked()), this, SLOT(generateAO()));
    generateAOButton->setEnabled(false);
    connect(this, SIGNAL(meshLoaded(bool)), generateAOButton, SLOT(setEnabled(bool)));

    QPushButton* screenshotButton = new QPushButton("Take Screenshot");
    connect(screenshotButton, SIGNAL(clicked()), this, SLOT(takeScreenshot()));
    screenshotButton->setEnabled(false);
    connect(this, SIGNAL(meshLoaded(bool)), screenshotButton, SLOT(setEnabled(bool)));

    QPushButton* saveButton = new QPushButton("Save .obj with AO");
    connect(saveButton, SIGNAL(clicked()), this, SLOT(saveMesh()));
    saveButton->setEnabled(false);
    connect(this, SIGNAL(aoLoaded(bool)), saveButton, SLOT(setEnabled(bool)));

    QPushButton* saveAOButton = new QPushButton("Save AO as dump");
    connect(saveAOButton, SIGNAL(clicked()), this, SLOT(saveAO()));
    saveAOButton->setEnabled(false);
    connect(this, SIGNAL(aoLoaded(bool)), saveAOButton, SLOT(setEnabled(bool)));

    renderWireframe_ = new QCheckBox("Render Wireframe");
    connect(renderWireframe_, SIGNAL(toggled(bool)), viewer_, SLOT(update()));

    renderAO_ = new QCheckBox("Render AO", this);
    renderAO_->setEnabled(false);
    connect(this, SIGNAL(aoLoaded(bool)), renderAO_, SLOT(setEnabled(bool)));
    connect(this, SIGNAL(aoLoaded(bool)), renderAO_, SLOT(setChecked(bool)));
    connect(renderAO_, SIGNAL(toggled(bool)), viewer_, SLOT(update()));

    renderLighting_ = new QCheckBox("Lighting", this);
    renderLighting_->setChecked(true);
    connect(renderLighting_, SIGNAL(toggled(bool)), viewer_, SLOT(update()));

    debugDepth_ = new QSpinBox();
    connect(debugDepth_, SIGNAL(valueChanged(int)), viewer_, SLOT(update()));
    
    meshLayout->addWidget(loadButton);
    meshLayout->addWidget(doBatchButton);
    meshLayout->addWidget(screenshotButton);
    meshLayout->addSpacing(3);
    meshLayout->addWidget(nSamplesLabel);
    meshLayout->addWidget(nSamples_);
    meshLayout->addWidget(distanceLabel);
    meshLayout->addWidget(maxDistance_);
    meshLayout->addWidget(generateAOButton);
    meshLayout->addSpacing(3);
    meshLayout->addWidget(saveButton);
    meshLayout->addWidget(saveAOButton);
    meshLayout->addSpacing(3);
    meshLayout->addWidget(renderWireframe_);
    meshLayout->addWidget(renderAO_);
    meshLayout->addWidget(renderLighting_);
    meshLayout->addWidget(debugDepth_);
    meshLayout->addStretch(20);
    meshBox->setLayout(meshLayout);

    rightBar_->setLayout(meshLayout);

}

void MainWindow::draw(){
    mesh_.draw(renderLighting_->isChecked(), renderAO_->isChecked());
    if ( renderWireframe_ ->isChecked()){
        mesh_.drawWireframe();
    }
    mesh_.drawKdTree(debugDepth_->value());
}

void MainWindow::initDraw()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    GLfloat LightPosition[] = {10, 10, 10, 1};
    glPushMatrix();
    glLoadIdentity();
    glLightfv (GL_LIGHT0, GL_POSITION, LightPosition);
    float amb = 0.1;
    float ambient[] = { amb, amb, amb, 1.0 };
    glLightfv( GL_LIGHT0, GL_AMBIENT, ambient);
    float diff = 0.5;
    float diffuse[] = { diff, diff, diff, 1.0 };
    glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse);
    float spec = 0.2;
    float specular[] = { spec, spec, spec, 1.0};
    glLightfv( GL_LIGHT0, GL_SPECULAR, specular);
    float white[] = { 1, 1, 1, 1.0};
    glMaterialfv(GL_FRONT, GL_SPECULAR, white);
    glPopMatrix();

    glMaterialf (GL_FRONT_AND_BACK, GL_SHININESS, 10.0f);
    glLineWidth(1.5);
    glEnable    (GL_BLEND);
    glEnable    (GL_LINE_SMOOTH);
    glHint      (GL_LINE_SMOOTH_HINT, GL_NICEST);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void MainWindow::loadMesh(){
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Mesh"), "/home/felix/Work/shapes", tr("Mesh Files (*.off *.obj *.ply)"));
    if( fileName.isNull() )
        return;

    bool succ = mesh_.load(fileName);


    if (succ){

        mesh_.mesh_.need_bbox();
        auto centr = mesh_.mesh_.bbox.center();
        qglviewer::Vec qcntr = qglviewer::Vec(centr.at(0), centr.at(1), centr.at(2));
        viewer_->setSceneCenter(qcntr);
        viewer_->setSceneRadius(mesh_.mesh_.bbox.radius());
        viewer_->showEntireScene();

        emit meshLoaded(true);
        viewer_->updateGL();
    }
}

void MainWindow::doBatch()
{
    int maxAO = 10;
    viewer_->setSnapshotFormat("JPEG");
    QFileDialog* fileDialog = new QFileDialog();
    QStringList fileNames = fileDialog->getOpenFileNames(this,
                                                    tr("Load Meshes"), "/home/felix/Work/shapes", tr("Mesh Files (*.off *.obj *.ply)"));
    fileDialog->close();
    if( fileNames.isEmpty() )
        return;

    viewer_->resize(1000,1000);
    for ( QString fileName : fileNames){
        qDebug() << fileName;

        bool succ = mesh_.load(fileName);


        if (succ){
            mesh_.mesh_.need_bbox();
            auto centr = mesh_.mesh_.bbox.center();
            qglviewer::Vec qcntr = qglviewer::Vec(centr.at(0), centr.at(1), centr.at(2));
            viewer_->setSceneCenter(qcntr);
            viewer_->setSceneRadius(0.9*mesh_.mesh_.bbox.radius());
            viewer_->showEntireScene();

            emit meshLoaded(true);
            renderLighting_->setChecked(true);
            renderAO_->setChecked(false);
            viewer_->updateGL();

            mesh_.buildKD();

            for ( int i = 0; i <= maxAO; i +=3){

                mesh_.genAO(nSamples_->value(), i);
                renderLighting_->setChecked(false);
                renderAO_->setChecked(true);

                QStringList list = fileName.split("/");
                QString saveFileName;
                for ( int s = 0; s < list.size() - 1 ; s++){
                    saveFileName +=  list[s] + "/" ;
                }
                saveFileName += "ao/";
                QStringList fn = list.back().split(".");
                saveFileName += fn[0] + "AO";
                if ( i > 0 ){
                    saveFileName += QString::number(i);
                }
                QString screenName = saveFileName + ".jpeg";
                saveFileName += "." + fn[1];

                viewer_->updateGL();
                viewer_->saveSnapshot(screenName, true);

                mesh_.mesh_.write(saveFileName.toStdString());

                qDebug() << saveFileName + " saved!";

            }
        }
    }
}

void MainWindow::generateAO()
{

    std::cout << "Start building KD-Tree " << std::endl;
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    mesh_.buildKD();
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>( t2 - t1 ).count();
    std::cout << "KD-Tree finished after: " << duration << " seconds" << std::endl;
    mesh_.genAO(nSamples_->value(), maxDistance_->value());
    std::chrono::high_resolution_clock::time_point t3 = std::chrono::high_resolution_clock::now();

    duration = std::chrono::duration_cast<std::chrono::seconds>( t3 - t2 ).count();
    std::cout << "AO finished after: " << duration << " seconds" << std::endl;

    emit aoLoaded(true);
    renderLighting_->setChecked(false);

    viewer_->updateGL();

}

void MainWindow::saveMesh(){
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Mesh"), QDir::homePath() );
    if( fileName.isNull() ){
        return;
    }
    bool written = mesh_.mesh_.write(fileName.toStdString());
    if ( written )
        qDebug() << "Saved to " << fileName << "\n";
}

void MainWindow::takeScreenshot()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Screenshot"), QDir::homePath() );
    if( fileName.isNull() ){
        return;
    }
    mesh_.takeScreenshot(fileName);
}

void MainWindow::saveAO()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Mesh with AO"), QDir::homePath() );
    if( fileName.isNull() ){
        return;
    }
    bool written = true;
    mesh_.writeAO(fileName);
    if ( written )
        qDebug() << "Saved to " << fileName << "\n";
}


